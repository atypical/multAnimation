//
//  STGradientLayerVC.m
//  MultAnimationOC
//
//  Created by Stan on 2017-04-27.
//  Copyright © 2017 stan. All rights reserved.
//

#import "STGradientLayerVC.h"

@interface STGradientLayerVC ()
@property(strong,nonatomic)CAGradientLayer *gradientLayer;
@end

@implementation STGradientLayerVC

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.


}

- (void)viewWillAppear:(BOOL)animated{
    self.gradientLayer = [[CAGradientLayer alloc] init];
    
    [self setupGradientLayer];
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)setupGradientLayer {

    self.gradientLayer.frame = self.view.bounds;

    //        设置颜色组。这里设置了黑色、蓝色、橙色、红色、绿色五种颜色
    self.gradientLayer.colors = @[(__bridge id)[UIColor blackColor].CGColor,(__bridge id)[UIColor blueColor].CGColor,(__bridge id)[UIColor orangeColor].CGColor,(__bridge id)[UIColor redColor].CGColor,(__bridge id)[UIColor greenColor].CGColor];


    //        根据起点指向终点的方向来渐变颜色,范围是0～1
    self.gradientLayer.startPoint = CGPointMake(0, 0);
    self.gradientLayer.endPoint = CGPointMake(1, 1);



    //        设置颜色分割线，范围是0～1
    self.gradientLayer.locations = @[@0.1,@0.5,@0.7,@0.75,@0.95];

    [self.view.layer addSublayer:self.gradientLayer];

}

@end
