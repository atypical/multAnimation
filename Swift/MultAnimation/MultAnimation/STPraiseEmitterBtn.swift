//
//  STPraiseEmitterBtn.swift
//  MultAnimation
//
//  Created by Stan on 2017-04-21.
//  Copyright © 2017 stan. All rights reserved.
//

import UIKit

class STPraiseEmitterBtn: UIButton {

    var explosionLayer:CAEmitterLayer
    
    
    override var isSelected: Bool{
        didSet{
//            执行动画
           explosionAni()
        }
    }
    

    override init(frame: CGRect) {
      explosionLayer = CAEmitterLayer.init()
        super.init(frame: frame)
        setupExplosion()
    }
    
    required init?(coder aDecoder: NSCoder) {
        explosionLayer = CAEmitterLayer.init()
        super.init(coder: aDecoder)
        setupExplosion()
//        fatalError("init(coder:) has not been implemented")
    }
    
    
//    MARK: - 创建发射层
    func setupExplosion(){
    let explosionCell = CAEmitterCell.init()
        explosionCell.name = "explosion"
//        设置粒子颜色alpha能改变的范围
        explosionCell.alphaRange = 0.10
//        粒子alpha的改变速度
        explosionCell.alphaSpeed = -1.0
//        粒子的生命周期
        explosionCell.lifetime = 0.7
//        粒子生命周期的范围
        explosionCell.lifetimeRange = 0.3
        
//        粒子发射的初始速度
        explosionCell.birthRate = 2500
//        粒子的速度
        explosionCell.velocity = 40.00
//        粒子速度范围
        explosionCell.velocityRange = 10.00
        
//        粒子的缩放比例
        explosionCell.scale = 0.03
//        缩放比例范围
        explosionCell.scaleRange = 0.02
        
//        粒子要展现的图片
        explosionCell.contents = UIImage(named: "sparkle")?.cgImage
        
        explosionLayer.name = "explosionLayer"
        
//        发射源的形状
        explosionLayer.emitterShape = kCAEmitterLayerCircle
//        发射模式
        explosionLayer.emitterMode = kCAEmitterLayerOutline
//        发射源大小
        explosionLayer.emitterSize = CGSize.init(width: 10, height: 0)
//        发射源包含的粒子
        explosionLayer.emitterCells = [explosionCell]
//        渲染模式
        explosionLayer.renderMode = kCAEmitterLayerOldestFirst
        explosionLayer.masksToBounds = false
        explosionLayer.birthRate = 0
//        发射位置
        explosionLayer.position = CGPoint.init(x: frame.size.width/2, y: frame.size.height/2)
        explosionLayer.zPosition = -1
        layer.addSublayer(explosionLayer)
        

    }

//发射的动画
    func explosionAni(){
     let animation = CAKeyframeAnimation(keyPath: "transform.scale")
        if isSelected {
            animation.values = [1.5,0.8,1.0,1.2,1.0]
            animation.duration = 0.5
            
            startAnimation()
        } else {
            animation.values = [0.8,1.0]
            animation.duration = 0.4
        }
        
        animation.calculationMode = kCAAnimationCubic
        layer.add(animation, forKey: "transform.scale")
    }
    
    func startAnimation(){

        
        explosionLayer.beginTime = CACurrentMediaTime()
        explosionLayer.birthRate = 1
        perform(#selector(STPraiseEmitterBtn.stopAnimation), with: nil, afterDelay: 0.15)
    }
    
    func stopAnimation(){
      explosionLayer.birthRate = 0
    }
}
