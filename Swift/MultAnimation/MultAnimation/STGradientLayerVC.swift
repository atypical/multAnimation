//
//  STGradientLayerVC.swift
//  MultAnimation
//
//  Created by Stan on 2017-04-21.
//  Copyright © 2017 stan. All rights reserved.
//

import UIKit

class STGradientLayerVC: UIViewController {
    
    var gradientlayer: CAGradientLayer!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        //创建gradientlayer
        createGradientLayer()
    }
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func createGradientLayer(){
        gradientlayer = CAGradientLayer.init()
        gradientlayer.frame = view.bounds
        
//        设置颜色组。这里设置了黑色、蓝色、橙色、红色、绿色五种颜色
        gradientlayer.colors = [UIColor.black.cgColor,UIColor.blue.cgColor,UIColor.orange.cgColor,UIColor.red.cgColor,UIColor.green.cgColor]
//        根据起点指向终点的方向来渐变颜色,范围是0～1
        gradientlayer.startPoint = CGPoint(x: 0.0, y: 0.0)
        gradientlayer.endPoint = CGPoint(x: 1.0, y: 1.0)
        
//        设置颜色分割线，范围是0～1
        gradientlayer.locations = [0.1,0.5,0.7,0.75,0.95]
        view.layer.addSublayer(gradientlayer)
    }
    
    
}
